import QtQuick 2.5
import QtQuick.Controls 1.0 as QtControls
import QtQuick.Controls 2.3 as QtControls2
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.3
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.kirigami 2.4 as Kirigami

Kirigami.FormLayout {
	id: root
	
	property alias cfg_Icon: icon.text
	property alias cfg_Colour: colour.text
	property alias cfg_Speed: speed.value

	Row {
		spacing: 2
		Kirigami.FormData.label: "Icon"
		QtControls.TextField {
			id: icon
		}
		QtControls.Button {
			text: "Select Icon"
			onClicked: iconDialog.open()
		}
	}
	
	Row{
		spacing: 2
		Kirigami.FormData.label: "Background Colour"
		QtControls.TextField {
			id: colour
		}
		QtControls.Button {
			text: "Select Colour"
			onClicked: colourDialog.open()
		}
	}
	
	Row{
		spacing: 2
		Kirigami.FormData.label: "Speed"
		QtControls2.Slider {
			id: speed
			from: 1
			to: 10
		}
	}
	
	FileDialog {
		id: iconDialog
		title: "Select and icon"
		nameFilters: [ "Image files (*.jpg *.png)" ]
		selectExisting: true
		selectFolder: false
		selectMultiple: false
		
		onAccepted: icon.text = iconDialog.fileUrl
	}
	
	ColorDialog {
		id: colourDialog
		title: "Select background colour"
		showAlphaChannel: false
		
		onAccepted: colour.text = colourDialog.color
	}
}
